package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

@RestController
public class Client2RestController {

	@Autowired
    private RestTemplate restTemplate;
	
/*	@Value("${sclient1.name}")
	private String client1;
*/
	@Autowired
    private EurekaClient eurekaClient;
	
	@RequestMapping("/get")
	public String index() {
		
		
		Application application = eurekaClient.getApplication("client1");
        InstanceInfo instanceInfo = application.getInstances().get(0);
        String url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + "/hello";
        System.out.println("URL: " + url);
        String result =restTemplate.getForObject(url, String.class);
        
		return "From client1: "+result;
	}
}
