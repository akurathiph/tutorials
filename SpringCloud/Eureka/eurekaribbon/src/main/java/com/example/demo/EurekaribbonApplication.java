package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableDiscoveryClient
@RibbonClient(name = "ribbonclient", configuration = RibbonConfiguration.class)
@SpringBootApplication
public class EurekaribbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaribbonApplication.class, args);
	}
	
	
	 @Bean
	 public RestTemplate restTemplate(RestTemplateBuilder builder) {
	        return builder.build();
	 }
	 
}
