package com.example.demo;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Client2RestController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private LoadBalancerClient loadBalancerClient;

	@RequestMapping("/get")
	public String index() throws Exception {

		String url = getEndPoint("client1") + "/hello";
		System.out.println("URL: " + url);
		String result = restTemplate.getForObject(url, String.class);

		return "From client1: " + result;
	}

	public String getEndPoint(String name) throws Exception {
		ServiceInstance instance = this.loadBalancerClient.choose(name);
		if (instance == null)
			throw new IllegalStateException(name + " is not registered in the service registry");
		URI uri = instance.getUri();
		return uri.toString();
	}

}
