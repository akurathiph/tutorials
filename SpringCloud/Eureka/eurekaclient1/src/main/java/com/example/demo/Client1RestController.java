package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Client1RestController {


	@RequestMapping("/hello")
	public String index() {
		return "Hi Team, Welcome to PCF tutorial";
	}

	
}
