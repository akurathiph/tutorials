package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigRestController {
	
	@Value("${hello.message}")
    String message;
	
	@RequestMapping("/hello")
	public String index() {
		return message;
	}


}
